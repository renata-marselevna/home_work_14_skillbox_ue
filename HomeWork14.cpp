

#include <iostream>
#include <string>

int main()
{
    std::string homework("Homework14");
    std::string firstSymbol, lastSymbol;
    firstSymbol = homework[0];
    lastSymbol = homework[homework.length() - 1];
    std::cout << homework << " " << homework.length() << " " << firstSymbol << " " << lastSymbol << "\n";
    return 0;
}
